const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

module.exports = {
  entry: {
    'app': './src/index.js'
  },
  output: {
    filename: '[name].js',
    path: __dirname + '/dist/'
  },
  mode: 'production',
  module: {
    rules: [{
      test: /\.scss$/,
      use: [
        MiniCssExtractPlugin.loader,
        'css-loader',
        'sass-loader'
      ]
    }]
  },
  'plugins': [
    // Extracts CSS into its own file, rather  than
    // coupling it in the javascript
    new MiniCssExtractPlugin({
      filename: '[name].css',
      path: __dirname + '/dist/'
    })
  ],
  'optimization': {
    minimizer: [
      // Minifies CSS files during the build process
      new OptimizeCSSAssetsPlugin({}),
      // Minifies JS fules during the build process
      new UglifyJsPlugin()
    ]
  }
}
